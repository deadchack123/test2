"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var webpack_1 = __importDefault(require("webpack"));
var webpack_merge_1 = __importDefault(require("webpack-merge"));
var webpack_base_config_1 = __importDefault(require("./webpack.base.config"));
var vue_ssr_webpack_plugin_1 = __importDefault(require("vue-ssr-webpack-plugin"));
// when output cache is enabled generate cache version key
var config_1 = __importDefault(require("config"));
var fs_1 = __importDefault(require("fs"));
var path_1 = __importDefault(require("path"));
var v4_1 = __importDefault(require("uuid/v4"));
if (config_1.default.server.useOutputCache) {
    fs_1.default.writeFileSync(path_1.default.join(__dirname, 'cache-version.json'), JSON.stringify(v4_1.default()));
}
exports.default = webpack_merge_1.default(webpack_base_config_1.default, {
    mode: 'development',
    target: 'node',
    entry: ['@babel/polyfill', './core/server-entry.ts'],
    output: {
        filename: 'server-bundle.js',
        libraryTarget: 'commonjs2'
    },
    resolve: {
        alias: {
            'create-api': './create-api-server.js'
        }
    },
    externals: Object.keys(require('../../package.json').dependencies),
    plugins: [
        new webpack_1.default.DefinePlugin({
            'process.env.VUE_ENV': '"server"'
        }),
        new vue_ssr_webpack_plugin_1.default()
    ]
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL2hvbWUvYW5kcmV5L3Byb2plY3QvdnVlLXN0b3JlZnJvbnQvdnVlLXN0b3JlZnJvbnQvY29yZS9idWlsZC93ZWJwYWNrLnNlcnZlci5jb25maWcudHMiLCJzb3VyY2VzIjpbIi9ob21lL2FuZHJleS9wcm9qZWN0L3Z1ZS1zdG9yZWZyb250L3Z1ZS1zdG9yZWZyb250L2NvcmUvYnVpbGQvd2VicGFjay5zZXJ2ZXIuY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsb0RBQThCO0FBQzlCLGdFQUFrQztBQUNsQyw4RUFBeUM7QUFDekMsa0ZBQWtEO0FBRWxELDBEQUEwRDtBQUMxRCxrREFBMkI7QUFDM0IsMENBQW1CO0FBQ25CLDhDQUF1QjtBQUN2QiwrQ0FBMEI7QUFFMUIsSUFBSSxnQkFBTSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUU7SUFDaEMsWUFBRSxDQUFDLGFBQWEsQ0FDZCxjQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxvQkFBb0IsQ0FBQyxFQUMxQyxJQUFJLENBQUMsU0FBUyxDQUFDLFlBQUksRUFBRSxDQUFDLENBQ3ZCLENBQUE7Q0FDRjtBQUVELGtCQUFlLHVCQUFLLENBQUMsNkJBQUksRUFBRTtJQUN6QixJQUFJLEVBQUUsYUFBYTtJQUNuQixNQUFNLEVBQUUsTUFBTTtJQUNkLEtBQUssRUFBRSxDQUFDLGlCQUFpQixFQUFFLHdCQUF3QixDQUFDO0lBQ3BELE1BQU0sRUFBRTtRQUNOLFFBQVEsRUFBRSxrQkFBa0I7UUFDNUIsYUFBYSxFQUFFLFdBQVc7S0FDM0I7SUFDRCxPQUFPLEVBQUU7UUFDUCxLQUFLLEVBQUU7WUFDTCxZQUFZLEVBQUUsd0JBQXdCO1NBQ3ZDO0tBQ0Y7SUFDRCxTQUFTLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsb0JBQW9CLENBQUMsQ0FBQyxZQUFZLENBQUM7SUFDbEUsT0FBTyxFQUFFO1FBQ1AsSUFBSSxpQkFBTyxDQUFDLFlBQVksQ0FBQztZQUN2QixxQkFBcUIsRUFBRSxVQUFVO1NBQ2xDLENBQUM7UUFDRixJQUFJLGdDQUFZLEVBQUU7S0FDbkI7Q0FDRixDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgd2VicGFjayBmcm9tICd3ZWJwYWNrJztcbmltcG9ydCBtZXJnZSBmcm9tICd3ZWJwYWNrLW1lcmdlJztcbmltcG9ydCBiYXNlIGZyb20gJy4vd2VicGFjay5iYXNlLmNvbmZpZyc7XG5pbXBvcnQgVnVlU1NSUGx1Z2luIGZyb20gJ3Z1ZS1zc3Itd2VicGFjay1wbHVnaW4nO1xuXG4vLyB3aGVuIG91dHB1dCBjYWNoZSBpcyBlbmFibGVkIGdlbmVyYXRlIGNhY2hlIHZlcnNpb24ga2V5XG5pbXBvcnQgY29uZmlnIGZyb20gJ2NvbmZpZydcbmltcG9ydCBmcyBmcm9tICdmcydcbmltcG9ydCBwYXRoIGZyb20gJ3BhdGgnXG5pbXBvcnQgdXVpZCBmcm9tICd1dWlkL3Y0J1xuXG5pZiAoY29uZmlnLnNlcnZlci51c2VPdXRwdXRDYWNoZSkge1xuICBmcy53cml0ZUZpbGVTeW5jKFxuICAgIHBhdGguam9pbihfX2Rpcm5hbWUsICdjYWNoZS12ZXJzaW9uLmpzb24nKSxcbiAgICBKU09OLnN0cmluZ2lmeSh1dWlkKCkpXG4gIClcbn1cblxuZXhwb3J0IGRlZmF1bHQgbWVyZ2UoYmFzZSwge1xuICBtb2RlOiAnZGV2ZWxvcG1lbnQnLFxuICB0YXJnZXQ6ICdub2RlJyxcbiAgZW50cnk6IFsnQGJhYmVsL3BvbHlmaWxsJywgJy4vY29yZS9zZXJ2ZXItZW50cnkudHMnXSxcbiAgb3V0cHV0OiB7XG4gICAgZmlsZW5hbWU6ICdzZXJ2ZXItYnVuZGxlLmpzJyxcbiAgICBsaWJyYXJ5VGFyZ2V0OiAnY29tbW9uanMyJ1xuICB9LFxuICByZXNvbHZlOiB7XG4gICAgYWxpYXM6IHtcbiAgICAgICdjcmVhdGUtYXBpJzogJy4vY3JlYXRlLWFwaS1zZXJ2ZXIuanMnXG4gICAgfVxuICB9LFxuICBleHRlcm5hbHM6IE9iamVjdC5rZXlzKHJlcXVpcmUoJy4uLy4uL3BhY2thZ2UuanNvbicpLmRlcGVuZGVuY2llcyksXG4gIHBsdWdpbnM6IFtcbiAgICBuZXcgd2VicGFjay5EZWZpbmVQbHVnaW4oe1xuICAgICAgJ3Byb2Nlc3MuZW52LlZVRV9FTlYnOiAnXCJzZXJ2ZXJcIidcbiAgICB9KSxcbiAgICBuZXcgVnVlU1NSUGx1Z2luKClcbiAgXVxufSlcbiJdfQ==