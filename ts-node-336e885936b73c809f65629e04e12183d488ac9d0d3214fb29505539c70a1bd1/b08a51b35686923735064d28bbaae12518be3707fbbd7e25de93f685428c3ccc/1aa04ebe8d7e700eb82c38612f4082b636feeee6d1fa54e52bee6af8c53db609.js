"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = __importDefault(require("path"));
var webpack_server_config_1 = __importDefault(require("./webpack.server.config"));
var theme_path_1 = __importDefault(require("./theme-path"));
var extendedConfig = require(path_1.default.join(theme_path_1.default, '/webpack.config.js'));
exports.default = extendedConfig(webpack_server_config_1.default, {
    mode: 'production',
    devtool: 'nosources-source-map',
    isClient: false,
    isDev: false
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL2hvbWUvYW5kcmV5L3Byb2plY3QvdnVlLXN0b3JlZnJvbnQvdnVlLXN0b3JlZnJvbnQvY29yZS9idWlsZC93ZWJwYWNrLnByb2Quc2VydmVyLmNvbmZpZy50cyIsInNvdXJjZXMiOlsiL2hvbWUvYW5kcmV5L3Byb2plY3QvdnVlLXN0b3JlZnJvbnQvdnVlLXN0b3JlZnJvbnQvY29yZS9idWlsZC93ZWJwYWNrLnByb2Quc2VydmVyLmNvbmZpZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7OztBQUFBLDhDQUF3QjtBQUV4QixrRkFBdUQ7QUFFdkQsNERBQXFDO0FBRXJDLElBQU0sY0FBYyxHQUFHLE9BQU8sQ0FBQyxjQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFTLEVBQUUsb0JBQW9CLENBQUMsQ0FBQyxDQUFBO0FBRTFFLGtCQUFlLGNBQWMsQ0FBQywrQkFBZ0IsRUFBRTtJQUM5QyxJQUFJLEVBQUUsWUFBWTtJQUNsQixPQUFPLEVBQUUsc0JBQXNCO0lBQy9CLFFBQVEsRUFBRSxLQUFLO0lBQ2YsS0FBSyxFQUFFLEtBQUs7Q0FDYixDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgcGF0aCBmcm9tICdwYXRoJztcblxuaW1wb3J0IGJhc2VTZXJ2ZXJDb25maWcgZnJvbSAnLi93ZWJwYWNrLnNlcnZlci5jb25maWcnO1xuXG5pbXBvcnQgdGhlbWVSb290IGZyb20gJy4vdGhlbWUtcGF0aCc7XG5cbmNvbnN0IGV4dGVuZGVkQ29uZmlnID0gcmVxdWlyZShwYXRoLmpvaW4odGhlbWVSb290LCAnL3dlYnBhY2suY29uZmlnLmpzJykpXG5cbmV4cG9ydCBkZWZhdWx0IGV4dGVuZGVkQ29uZmlnKGJhc2VTZXJ2ZXJDb25maWcsIHtcbiAgbW9kZTogJ3Byb2R1Y3Rpb24nLFxuICBkZXZ0b29sOiAnbm9zb3VyY2VzLXNvdXJjZS1tYXAnLCAgXG4gIGlzQ2xpZW50OiBmYWxzZSxcbiAgaXNEZXY6IGZhbHNlXG59KVxuIl19