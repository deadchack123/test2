"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var webpack_1 = __importDefault(require("webpack"));
var webpack_merge_1 = __importDefault(require("webpack-merge"));
var webpack_base_config_1 = __importDefault(require("./webpack.base.config"));
var sw_precache_webpack_plugin_1 = __importDefault(require("sw-precache-webpack-plugin"));
module.exports = webpack_merge_1.default(webpack_base_config_1.default, {
    mode: 'production',
    target: 'web',
    entry: ['@babel/polyfill', './core/service-worker/index.js'],
    output: {
        filename: 'core-service-worker.js'
    },
    plugins: [
        new webpack_1.default.DefinePlugin({
            'process.env.VUE_ENV': '"client"'
        }),
        // auto generate service worker
        new sw_precache_webpack_plugin_1.default({
            cacheId: 'vue-sfr',
            filename: 'service-worker.js',
            staticFileGlobsIgnorePatterns: [/\.map$/],
            staticFileGlobs: [
                'dist/**.*',
                'assets/**.*',
                'assets/ig/**.*',
                'index.html',
                '/'
            ],
            runtimeCaching: [
                {
                    urlPattern: "^https://fonts\.googleapis\.com/",
                    handler: "cacheFirst"
                },
                {
                    urlPattern: "^https://fonts\.gstatic\.com/",
                    handler: "cacheFirst"
                },
                {
                    urlPattern: "^https://unpkg\.com/",
                    handler: "cacheFirst"
                },
                {
                    urlPattern: "/pwa.html",
                    handler: "networkFirst"
                }, {
                    urlPattern: "/",
                    handler: "networkFirst"
                },
                {
                    urlPattern: "/p/*",
                    handler: "networkFirst"
                },
                {
                    urlPattern: "/c/*",
                    handler: "networkFirst"
                },
                {
                    urlPattern: "/img/(.*)",
                    handler: "fastest"
                }, {
                    urlPattern: "/api/catalog/*",
                    handler: "networkFirst"
                }, {
                    urlPattern: "/api/*",
                    handler: "networkFirst"
                }, {
                    urlPattern: "/assets/logo.svg",
                    handler: "networkFirst"
                }, {
                    urlPattern: "/index.html",
                    handler: "networkFirst"
                }, {
                    urlPattern: "/assets/*",
                    handler: "fastest"
                }, {
                    urlPattern: "/assets/ig/(.*)",
                    handler: "fastest"
                }, {
                    urlPattern: "/dist/(.*)",
                    handler: "fastest"
                }, {
                    urlPattern: "/*/*",
                    handler: "networkFirst"
                },
                {
                    urlPattern: "/*/*/*",
                    handler: "networkFirst"
                },
                {
                    urlPattern: "/*",
                    handler: "networkFirst"
                }
            ],
            "importScripts": ['/dist/core-service-worker.js'] /* custom logic */
        })
    ]
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL2hvbWUvYW5kcmV5L3Byb2plY3QvdnVlLXN0b3JlZnJvbnQvdnVlLXN0b3JlZnJvbnQvY29yZS9idWlsZC93ZWJwYWNrLnByb2Quc3cuY29uZmlnLnRzIiwic291cmNlcyI6WyIvaG9tZS9hbmRyZXkvcHJvamVjdC92dWUtc3RvcmVmcm9udC92dWUtc3RvcmVmcm9udC9jb3JlL2J1aWxkL3dlYnBhY2sucHJvZC5zdy5jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxvREFBOEI7QUFDOUIsZ0VBQWtDO0FBQ2xDLDhFQUF5QztBQUN6QywwRkFBMEQ7QUFFMUQsTUFBTSxDQUFDLE9BQU8sR0FBRyx1QkFBSyxDQUFDLDZCQUFJLEVBQUU7SUFDM0IsSUFBSSxFQUFFLFlBQVk7SUFDbEIsTUFBTSxFQUFFLEtBQUs7SUFDYixLQUFLLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSxnQ0FBZ0MsQ0FBQztJQUM1RCxNQUFNLEVBQUU7UUFDTixRQUFRLEVBQUUsd0JBQXdCO0tBQ25DO0lBQ0QsT0FBTyxFQUFFO1FBQ1AsSUFBSSxpQkFBTyxDQUFDLFlBQVksQ0FBQztZQUN2QixxQkFBcUIsRUFBRSxVQUFVO1NBQ2xDLENBQUM7UUFDRiwrQkFBK0I7UUFDL0IsSUFBSSxvQ0FBZ0IsQ0FBQztZQUNuQixPQUFPLEVBQUUsU0FBUztZQUNsQixRQUFRLEVBQUUsbUJBQW1CO1lBQzdCLDZCQUE2QixFQUFFLENBQUMsUUFBUSxDQUFDO1lBQ3pDLGVBQWUsRUFBRTtnQkFDZixXQUFXO2dCQUNYLGFBQWE7Z0JBQ2IsZ0JBQWdCO2dCQUNoQixZQUFZO2dCQUNaLEdBQUc7YUFDSjtZQUNELGNBQWMsRUFBRTtnQkFDZDtvQkFDRSxVQUFVLEVBQUUsa0NBQWtDO29CQUM5QyxPQUFPLEVBQUUsWUFBWTtpQkFDdEI7Z0JBQ0Q7b0JBQ0UsVUFBVSxFQUFFLCtCQUErQjtvQkFDM0MsT0FBTyxFQUFFLFlBQVk7aUJBQ3RCO2dCQUNEO29CQUNFLFVBQVUsRUFBRSxzQkFBc0I7b0JBQ2xDLE9BQU8sRUFBRSxZQUFZO2lCQUN0QjtnQkFDRDtvQkFDQSxVQUFVLEVBQUUsV0FBVztvQkFDdkIsT0FBTyxFQUFFLGNBQWM7aUJBQ3hCLEVBQUM7b0JBQ0EsVUFBVSxFQUFFLEdBQUc7b0JBQ2YsT0FBTyxFQUFFLGNBQWM7aUJBQ3hCO2dCQUNEO29CQUNFLFVBQVUsRUFBRSxNQUFNO29CQUNsQixPQUFPLEVBQUUsY0FBYztpQkFDeEI7Z0JBQ0Q7b0JBQ0UsVUFBVSxFQUFFLE1BQU07b0JBQ2xCLE9BQU8sRUFBRSxjQUFjO2lCQUN4QjtnQkFDRDtvQkFDRSxVQUFVLEVBQUUsV0FBVztvQkFDdkIsT0FBTyxFQUFFLFNBQVM7aUJBQ25CLEVBQUM7b0JBQ0EsVUFBVSxFQUFFLGdCQUFnQjtvQkFDNUIsT0FBTyxFQUFFLGNBQWM7aUJBQ3hCLEVBQUM7b0JBQ0EsVUFBVSxFQUFFLFFBQVE7b0JBQ3BCLE9BQU8sRUFBRSxjQUFjO2lCQUN4QixFQUFDO29CQUNBLFVBQVUsRUFBRSxrQkFBa0I7b0JBQzlCLE9BQU8sRUFBRSxjQUFjO2lCQUN4QixFQUFDO29CQUNBLFVBQVUsRUFBRSxhQUFhO29CQUN6QixPQUFPLEVBQUUsY0FBYztpQkFDeEIsRUFBQztvQkFDQSxVQUFVLEVBQUUsV0FBVztvQkFDdkIsT0FBTyxFQUFFLFNBQVM7aUJBQ25CLEVBQUM7b0JBQ0EsVUFBVSxFQUFFLGlCQUFpQjtvQkFDN0IsT0FBTyxFQUFFLFNBQVM7aUJBQ25CLEVBQUM7b0JBQ0EsVUFBVSxFQUFFLFlBQVk7b0JBQ3hCLE9BQU8sRUFBRSxTQUFTO2lCQUNuQixFQUFDO29CQUNBLFVBQVUsRUFBRSxNQUFNO29CQUNsQixPQUFPLEVBQUUsY0FBYztpQkFDeEI7Z0JBQ0Q7b0JBQ0UsVUFBVSxFQUFFLFFBQVE7b0JBQ3BCLE9BQU8sRUFBRSxjQUFjO2lCQUN4QjtnQkFDRDtvQkFDRSxVQUFVLEVBQUUsSUFBSTtvQkFDaEIsT0FBTyxFQUFFLGNBQWM7aUJBQ3hCO2FBQUM7WUFDRixlQUFlLEVBQUUsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDLGtCQUFrQjtTQUNyRSxDQUFDO0tBQ0g7Q0FDRixDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgd2VicGFjayBmcm9tICd3ZWJwYWNrJztcbmltcG9ydCBtZXJnZSBmcm9tICd3ZWJwYWNrLW1lcmdlJztcbmltcG9ydCBiYXNlIGZyb20gJy4vd2VicGFjay5iYXNlLmNvbmZpZyc7XG5pbXBvcnQgU1dQcmVjYWNoZVBsdWdpbiBmcm9tICdzdy1wcmVjYWNoZS13ZWJwYWNrLXBsdWdpbic7XG5cbm1vZHVsZS5leHBvcnRzID0gbWVyZ2UoYmFzZSwge1xuICBtb2RlOiAncHJvZHVjdGlvbicsXG4gIHRhcmdldDogJ3dlYicsXG4gIGVudHJ5OiBbJ0BiYWJlbC9wb2x5ZmlsbCcsICcuL2NvcmUvc2VydmljZS13b3JrZXIvaW5kZXguanMnXSxcbiAgb3V0cHV0OiB7XG4gICAgZmlsZW5hbWU6ICdjb3JlLXNlcnZpY2Utd29ya2VyLmpzJ1xuICB9LFxuICBwbHVnaW5zOiBbXG4gICAgbmV3IHdlYnBhY2suRGVmaW5lUGx1Z2luKHtcbiAgICAgICdwcm9jZXNzLmVudi5WVUVfRU5WJzogJ1wiY2xpZW50XCInXG4gICAgfSksXG4gICAgLy8gYXV0byBnZW5lcmF0ZSBzZXJ2aWNlIHdvcmtlclxuICAgIG5ldyBTV1ByZWNhY2hlUGx1Z2luKHtcbiAgICAgIGNhY2hlSWQ6ICd2dWUtc2ZyJyxcbiAgICAgIGZpbGVuYW1lOiAnc2VydmljZS13b3JrZXIuanMnLFxuICAgICAgc3RhdGljRmlsZUdsb2JzSWdub3JlUGF0dGVybnM6IFsvXFwubWFwJC9dLFxuICAgICAgc3RhdGljRmlsZUdsb2JzOiBbXG4gICAgICAgICdkaXN0LyoqLionLFxuICAgICAgICAnYXNzZXRzLyoqLionLFxuICAgICAgICAnYXNzZXRzL2lnLyoqLionLFxuICAgICAgICAnaW5kZXguaHRtbCcsXG4gICAgICAgICcvJ1xuICAgICAgXSxcbiAgICAgIHJ1bnRpbWVDYWNoaW5nOiBbXG4gICAgICAgIHtcbiAgICAgICAgICB1cmxQYXR0ZXJuOiBcIl5odHRwczovL2ZvbnRzXFwuZ29vZ2xlYXBpc1xcLmNvbS9cIiwgLyoqIGNhY2hlIHRoZSBodG1sIHN0dWIgICovXG4gICAgICAgICAgaGFuZGxlcjogXCJjYWNoZUZpcnN0XCJcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgIHVybFBhdHRlcm46IFwiXmh0dHBzOi8vZm9udHNcXC5nc3RhdGljXFwuY29tL1wiLCAvKiogY2FjaGUgdGhlIGh0bWwgc3R1YiAgKi9cbiAgICAgICAgICBoYW5kbGVyOiBcImNhY2hlRmlyc3RcIlxuICAgICAgICB9LFxuICAgICAgICB7XG4gICAgICAgICAgdXJsUGF0dGVybjogXCJeaHR0cHM6Ly91bnBrZ1xcLmNvbS9cIiwgLyoqIGNhY2hlIHRoZSBodG1sIHN0dWIgICovXG4gICAgICAgICAgaGFuZGxlcjogXCJjYWNoZUZpcnN0XCJcbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICB1cmxQYXR0ZXJuOiBcIi9wd2EuaHRtbFwiLCAvKiogY2FjaGUgdGhlIGh0bWwgc3R1YiAgKi9cbiAgICAgICAgaGFuZGxlcjogXCJuZXR3b3JrRmlyc3RcIlxuICAgICAgfSx7XG4gICAgICAgIHVybFBhdHRlcm46IFwiL1wiLCAvKiogY2FjaGUgdGhlIGh0bWwgc3R1YiBmb3IgaG9tZXBhZ2UgICovXG4gICAgICAgIGhhbmRsZXI6IFwibmV0d29ya0ZpcnN0XCJcbiAgICAgIH0sXG4gICAgICB7XG4gICAgICAgIHVybFBhdHRlcm46IFwiL3AvKlwiLCAvKiogY2FjaGUgdGhlIGh0bWwgc3R1YiAgKi9cbiAgICAgICAgaGFuZGxlcjogXCJuZXR3b3JrRmlyc3RcIlxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdXJsUGF0dGVybjogXCIvYy8qXCIsIC8qKiBjYWNoZSB0aGUgaHRtbCBzdHViICAqL1xuICAgICAgICBoYW5kbGVyOiBcIm5ldHdvcmtGaXJzdFwiXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB1cmxQYXR0ZXJuOiBcIi9pbWcvKC4qKVwiLFxuICAgICAgICBoYW5kbGVyOiBcImZhc3Rlc3RcIlxuICAgICAgfSx7XG4gICAgICAgIHVybFBhdHRlcm46IFwiL2FwaS9jYXRhbG9nLypcIixcbiAgICAgICAgaGFuZGxlcjogXCJuZXR3b3JrRmlyc3RcIlxuICAgICAgfSx7XG4gICAgICAgIHVybFBhdHRlcm46IFwiL2FwaS8qXCIsXG4gICAgICAgIGhhbmRsZXI6IFwibmV0d29ya0ZpcnN0XCJcbiAgICAgIH0se1xuICAgICAgICB1cmxQYXR0ZXJuOiBcIi9hc3NldHMvbG9nby5zdmdcIixcbiAgICAgICAgaGFuZGxlcjogXCJuZXR3b3JrRmlyc3RcIlxuICAgICAgfSx7XG4gICAgICAgIHVybFBhdHRlcm46IFwiL2luZGV4Lmh0bWxcIixcbiAgICAgICAgaGFuZGxlcjogXCJuZXR3b3JrRmlyc3RcIlxuICAgICAgfSx7XG4gICAgICAgIHVybFBhdHRlcm46IFwiL2Fzc2V0cy8qXCIsXG4gICAgICAgIGhhbmRsZXI6IFwiZmFzdGVzdFwiXG4gICAgICB9LHtcbiAgICAgICAgdXJsUGF0dGVybjogXCIvYXNzZXRzL2lnLyguKilcIixcbiAgICAgICAgaGFuZGxlcjogXCJmYXN0ZXN0XCJcbiAgICAgIH0se1xuICAgICAgICB1cmxQYXR0ZXJuOiBcIi9kaXN0LyguKilcIixcbiAgICAgICAgaGFuZGxlcjogXCJmYXN0ZXN0XCJcbiAgICAgIH0se1xuICAgICAgICB1cmxQYXR0ZXJuOiBcIi8qLypcIiwgLyoqIHRoaXMgaXMgbmV3IHByb2R1Y3QgVVJMIGZvcm1hdCAgKi9cbiAgICAgICAgaGFuZGxlcjogXCJuZXR3b3JrRmlyc3RcIlxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdXJsUGF0dGVybjogXCIvKi8qLypcIiwgLyoqIHRoaXMgaXMgbmV3IHByb2R1Y3QgVVJMIGZvcm1hdCAgKi9cbiAgICAgICAgaGFuZGxlcjogXCJuZXR3b3JrRmlyc3RcIlxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdXJsUGF0dGVybjogXCIvKlwiLCAvKiogdGhpcyBpcyBuZXcgY2F0ZWdvcnkgVVJMIGZvcm1hdCAgKi9cbiAgICAgICAgaGFuZGxlcjogXCJuZXR3b3JrRmlyc3RcIlxuICAgICAgfV0sXG4gICAgICBcImltcG9ydFNjcmlwdHNcIjogWycvZGlzdC9jb3JlLXNlcnZpY2Utd29ya2VyLmpzJ10gLyogY3VzdG9tIGxvZ2ljICovXG4gICAgfSlcbiAgXVxufSlcbiJdfQ==