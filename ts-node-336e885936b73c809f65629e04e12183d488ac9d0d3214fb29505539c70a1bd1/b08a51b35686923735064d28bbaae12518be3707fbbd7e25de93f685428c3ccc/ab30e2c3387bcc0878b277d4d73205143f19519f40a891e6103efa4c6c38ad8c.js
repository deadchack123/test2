"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var path_1 = __importDefault(require("path"));
var config_1 = __importDefault(require("config"));
var fs_1 = __importDefault(require("fs"));
var case_sensitive_paths_webpack_plugin_1 = __importDefault(require("case-sensitive-paths-webpack-plugin"));
var plugin_1 = __importDefault(require("vue-loader/lib/plugin"));
var autoprefixer_1 = __importDefault(require("autoprefixer"));
var html_webpack_plugin_1 = __importDefault(require("html-webpack-plugin"));
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;
var webpack_1 = __importDefault(require("webpack"));
var moment_1 = __importDefault(require("moment"));
fs_1.default.writeFileSync(path_1.default.resolve(__dirname, './config.json'), JSON.stringify(config_1.default));
var themesRoot = '../../src/themes';
var theme_path_1 = __importDefault(require("./theme-path"));
var themeResources = theme_path_1.default + '/resource';
var themeCSS = theme_path_1.default + '/css';
var themeApp = theme_path_1.default + '/App.vue';
var themedIndex = path_1.default.join(theme_path_1.default, '/templates/index.template.html');
var themedIndexMinimal = path_1.default.join(theme_path_1.default, '/templates/index.minimal.template.html');
var themedIndexBasic = path_1.default.join(theme_path_1.default, '/templates/index.basic.template.html');
var themedIndexAmp = path_1.default.join(theme_path_1.default, '/templates/index.amp.template.html');
var translationPreprocessor = require('@vue-storefront/i18n/scripts/translation.preprocessor.js');
translationPreprocessor([
    path_1.default.resolve(__dirname, '../../node_modules/@vue-storefront/i18n/resource/i18n/'),
    path_1.default.resolve(__dirname, themeResources + '/i18n/')
], config_1.default);
var postcssConfig = {
    loader: 'postcss-loader',
    options: {
        ident: 'postcss',
        plugins: function (loader) { return [
            require('postcss-flexbugs-fixes'),
            require('autoprefixer')({
                flexbox: 'no-2009',
            }),
        ]; }
    }
};
var isProd = process.env.NODE_ENV === 'production';
// todo: usemultipage-webpack-plugin for multistore
exports.default = {
    plugins: [
        new webpack_1.default.ProgressPlugin(),
        // new BundleAnalyzerPlugin({
        //   generateStatsFile: true
        // }),
        new case_sensitive_paths_webpack_plugin_1.default(),
        new plugin_1.default(),
        // generate output HTML
        new html_webpack_plugin_1.default({
            template: fs_1.default.existsSync(themedIndex) ? themedIndex : 'src/index.template.html',
            filename: 'index.html',
            chunksSortMode: 'none',
            inject: isProd == false // in dev mode we're not using clientManifest therefore renderScripts() is returning empty string and we need to inject scripts using HTMLPlugin
        }),
        new html_webpack_plugin_1.default({
            template: fs_1.default.existsSync(themedIndexMinimal) ? themedIndexMinimal : 'src/index.minimal.template.html',
            filename: 'index.minimal.html',
            chunksSortMode: 'none',
            inject: isProd == false
        }),
        new html_webpack_plugin_1.default({
            template: fs_1.default.existsSync(themedIndexBasic) ? themedIndexBasic : 'src/index.basic.template.html',
            filename: 'index.basic.html',
            chunksSortMode: 'none',
            inject: isProd == false
        }),
        new html_webpack_plugin_1.default({
            template: fs_1.default.existsSync(themedIndexAmp) ? themedIndexAmp : 'src/index.amp.template.html',
            filename: 'index.amp.html',
            chunksSortMode: 'none',
            inject: isProd == false
        }),
        new webpack_1.default.DefinePlugin({
            'process.env.__APPVERSION__': JSON.stringify(require('../../package.json').version),
            'process.env.__BUILDTIME__': JSON.stringify(moment_1.default().format('YYYY-MM-DD HH:mm:ss'))
        })
    ],
    devtool: 'source-map',
    entry: {
        app: ['@babel/polyfill', './core/client-entry.ts']
    },
    output: {
        path: path_1.default.resolve(__dirname, '../../dist'),
        publicPath: '/dist/',
        filename: '[name].[hash].js'
    },
    resolveLoader: {
        modules: [
            'node_modules',
            path_1.default.resolve(__dirname, themesRoot)
        ],
    },
    resolve: {
        modules: [
            'node_modules',
            path_1.default.resolve(__dirname, themesRoot)
        ],
        extensions: ['.js', '.vue', '.gql', '.graphqls', '.ts'],
        alias: {
            // Main aliases
            'config': path_1.default.resolve(__dirname, './config.json'),
            'src': path_1.default.resolve(__dirname, '../../src'),
            // Theme aliases
            'theme': theme_path_1.default,
            'theme/app': themeApp,
            'theme/css': themeCSS,
            'theme/resource': themeResources,
            // Backward compatible
            '@vue-storefront/core/store/lib/multistore': path_1.default.resolve(__dirname, '../lib/multistore.ts'),
        }
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.(js|vue)$/,
                loader: 'eslint-loader',
                exclude: [/node_modules/, /test/]
            },
            {
                test: /\.vue$/,
                loader: 'vue-loader',
                options: {
                    preserveWhitespace: false,
                    postcss: [autoprefixer_1.default()],
                }
            },
            {
                test: /\.ts$/,
                loader: 'ts-loader',
                options: {
                    appendTsSuffixTo: [/\.vue$/]
                },
                exclude: /node_modules/
            },
            {
                test: /\.js$/,
                loader: 'babel-loader',
                include: [
                    path_1.default.resolve(__dirname, '../../node_modules/@vue-storefront'),
                    path_1.default.resolve(__dirname, '../../src'),
                    path_1.default.resolve(__dirname, '../../core')
                ]
            },
            {
                test: /\.(png|jpg|gif|svg)$/,
                loader: 'file-loader',
                options: {
                    name: '[name].[ext]?[hash]'
                }
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    postcssConfig
                ]
            },
            {
                test: /\.scss$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    postcssConfig,
                    'sass-loader'
                ]
            },
            {
                test: /\.sass$/,
                use: [
                    'vue-style-loader',
                    'css-loader',
                    postcssConfig,
                    {
                        loader: 'sass-loader',
                        options: {
                            indentedSyntax: true
                        }
                    }
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf)(\?.*$|$)/,
                loader: 'url-loader?importLoaders=1&limit=10000'
            },
            {
                test: /\.(graphqls|gql)$/,
                exclude: /node_modules/,
                loader: ['graphql-tag/loader']
            }
        ]
    }
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL2hvbWUvYW5kcmV5L3Byb2plY3QvdnVlLXN0b3JlZnJvbnQvdnVlLXN0b3JlZnJvbnQvY29yZS9idWlsZC93ZWJwYWNrLmJhc2UuY29uZmlnLnRzIiwic291cmNlcyI6WyIvaG9tZS9hbmRyZXkvcHJvamVjdC92dWUtc3RvcmVmcm9udC92dWUtc3RvcmVmcm9udC9jb3JlL2J1aWxkL3dlYnBhY2suYmFzZS5jb25maWcudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSw4Q0FBd0I7QUFDeEIsa0RBQTRCO0FBQzVCLDBDQUFvQjtBQUNwQiw0R0FBMkU7QUFDM0UsaUVBQW9EO0FBQ3BELDhEQUF3QztBQUN4Qyw0RUFBNkM7QUFDN0Msd0ZBQXdGO0FBQ3hGLG9EQUE4QjtBQUM5QixrREFBNEI7QUFFNUIsWUFBRSxDQUFDLGFBQWEsQ0FDZCxjQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxlQUFlLENBQUMsRUFDeEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBTSxDQUFDLENBQ3ZCLENBQUE7QUFFRCxJQUFNLFVBQVUsR0FBRyxrQkFBa0IsQ0FBQTtBQUVyQyw0REFBcUM7QUFDckMsSUFBTSxjQUFjLEdBQUcsb0JBQVMsR0FBRyxXQUFXLENBQUE7QUFDOUMsSUFBTSxRQUFRLEdBQUcsb0JBQVMsR0FBRyxNQUFNLENBQUE7QUFDbkMsSUFBTSxRQUFRLEdBQUcsb0JBQVMsR0FBRyxVQUFVLENBQUE7QUFDdkMsSUFBTSxXQUFXLEdBQUcsY0FBSSxDQUFDLElBQUksQ0FBQyxvQkFBUyxFQUFFLGdDQUFnQyxDQUFDLENBQUE7QUFDMUUsSUFBTSxrQkFBa0IsR0FBRyxjQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFTLEVBQUUsd0NBQXdDLENBQUMsQ0FBQTtBQUN6RixJQUFNLGdCQUFnQixHQUFHLGNBQUksQ0FBQyxJQUFJLENBQUMsb0JBQVMsRUFBRSxzQ0FBc0MsQ0FBQyxDQUFBO0FBQ3JGLElBQU0sY0FBYyxHQUFHLGNBQUksQ0FBQyxJQUFJLENBQUMsb0JBQVMsRUFBRSxvQ0FBb0MsQ0FBQyxDQUFBO0FBRWpGLElBQU0sdUJBQXVCLEdBQUcsT0FBTyxDQUFDLDBEQUEwRCxDQUFDLENBQUE7QUFDbkcsdUJBQXVCLENBQUM7SUFDdEIsY0FBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsd0RBQXdELENBQUM7SUFDakYsY0FBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsY0FBYyxHQUFHLFFBQVEsQ0FBQztDQUNuRCxFQUFFLGdCQUFNLENBQUMsQ0FBQTtBQUVWLElBQU0sYUFBYSxHQUFJO0lBQ3JCLE1BQU0sRUFBRSxnQkFBZ0I7SUFDeEIsT0FBTyxFQUFFO1FBQ1AsS0FBSyxFQUFFLFNBQVM7UUFDaEIsT0FBTyxFQUFFLFVBQUMsTUFBTSxJQUFLLE9BQUE7WUFDbkIsT0FBTyxDQUFDLHdCQUF3QixDQUFDO1lBQ2pDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztnQkFDdEIsT0FBTyxFQUFFLFNBQVM7YUFDbkIsQ0FBQztTQUNILEVBTG9CLENBS3BCO0tBQ0Y7Q0FDRixDQUFDO0FBQ0YsSUFBTSxNQUFNLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLEtBQUssWUFBWSxDQUFBO0FBQ3BELG1EQUFtRDtBQUNuRCxrQkFBZTtJQUNiLE9BQU8sRUFBRTtRQUNQLElBQUksaUJBQU8sQ0FBQyxjQUFjLEVBQUU7UUFDNUIsNkJBQTZCO1FBQzdCLDRCQUE0QjtRQUM1QixNQUFNO1FBQ04sSUFBSSw2Q0FBd0IsRUFBRTtRQUM5QixJQUFJLGdCQUFlLEVBQUU7UUFDckIsdUJBQXVCO1FBQ3ZCLElBQUksNkJBQVUsQ0FBQztZQUNiLFFBQVEsRUFBRSxZQUFFLENBQUMsVUFBVSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLHlCQUF5QjtZQUM5RSxRQUFRLEVBQUUsWUFBWTtZQUN0QixjQUFjLEVBQUUsTUFBTTtZQUN0QixNQUFNLEVBQUUsTUFBTSxJQUFJLEtBQUssQ0FBQyxnSkFBZ0o7U0FDekssQ0FBQztRQUNGLElBQUksNkJBQVUsQ0FBQztZQUNiLFFBQVEsRUFBRSxZQUFFLENBQUMsVUFBVSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQyxpQ0FBaUM7WUFDcEcsUUFBUSxFQUFFLG9CQUFvQjtZQUM5QixjQUFjLEVBQUUsTUFBTTtZQUN0QixNQUFNLEVBQUUsTUFBTSxJQUFJLEtBQUs7U0FDeEIsQ0FBQztRQUNGLElBQUksNkJBQVUsQ0FBQztZQUNiLFFBQVEsRUFBRSxZQUFFLENBQUMsVUFBVSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixDQUFBLENBQUMsQ0FBQywrQkFBK0I7WUFDN0YsUUFBUSxFQUFFLGtCQUFrQjtZQUM1QixjQUFjLEVBQUUsTUFBTTtZQUN0QixNQUFNLEVBQUUsTUFBTSxJQUFJLEtBQUs7U0FDeEIsQ0FBQztRQUNGLElBQUksNkJBQVUsQ0FBQztZQUNiLFFBQVEsRUFBRSxZQUFFLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUEsQ0FBQyxDQUFDLDZCQUE2QjtZQUN2RixRQUFRLEVBQUUsZ0JBQWdCO1lBQzFCLGNBQWMsRUFBRSxNQUFNO1lBQ3RCLE1BQU0sRUFBRSxNQUFNLElBQUksS0FBSztTQUN4QixDQUFDO1FBQ0YsSUFBSSxpQkFBTyxDQUFDLFlBQVksQ0FBQztZQUN2Qiw0QkFBNEIsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDLE9BQU8sQ0FBQztZQUNuRiwyQkFBMkIsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFNLEVBQUUsQ0FBQyxNQUFNLENBQUMscUJBQXFCLENBQUMsQ0FBQztTQUNwRixDQUFDO0tBQ0g7SUFDRCxPQUFPLEVBQUUsWUFBWTtJQUNyQixLQUFLLEVBQUU7UUFDTCxHQUFHLEVBQUUsQ0FBQyxpQkFBaUIsRUFBRSx3QkFBd0IsQ0FBQztLQUNuRDtJQUNELE1BQU0sRUFBRTtRQUNOLElBQUksRUFBRSxjQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxZQUFZLENBQUM7UUFDM0MsVUFBVSxFQUFFLFFBQVE7UUFDcEIsUUFBUSxFQUFFLGtCQUFrQjtLQUM3QjtJQUNELGFBQWEsRUFBRTtRQUNiLE9BQU8sRUFBRTtZQUNQLGNBQWM7WUFDZCxjQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUM7U0FDcEM7S0FDRjtJQUNELE9BQU8sRUFBRTtRQUNQLE9BQU8sRUFBRTtZQUNQLGNBQWM7WUFDZCxjQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxVQUFVLENBQUM7U0FDcEM7UUFDRCxVQUFVLEVBQUUsQ0FBQyxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxXQUFXLEVBQUUsS0FBSyxDQUFDO1FBQ3ZELEtBQUssRUFBRTtZQUNMLGVBQWU7WUFDZixRQUFRLEVBQUUsY0FBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsZUFBZSxDQUFDO1lBQ2xELEtBQUssRUFBRSxjQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxXQUFXLENBQUM7WUFFM0MsZ0JBQWdCO1lBQ2hCLE9BQU8sRUFBRSxvQkFBUztZQUNsQixXQUFXLEVBQUUsUUFBUTtZQUNyQixXQUFXLEVBQUUsUUFBUTtZQUNyQixnQkFBZ0IsRUFBRSxjQUFjO1lBRWhDLHNCQUFzQjtZQUN0QiwyQ0FBMkMsRUFBRyxjQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxzQkFBc0IsQ0FBQztTQUM5RjtLQUNGO0lBQ0QsTUFBTSxFQUFFO1FBQ04sS0FBSyxFQUFFO1lBQ0w7Z0JBQ0UsT0FBTyxFQUFFLEtBQUs7Z0JBQ2QsSUFBSSxFQUFFLGFBQWE7Z0JBQ25CLE1BQU0sRUFBRSxlQUFlO2dCQUN2QixPQUFPLEVBQUUsQ0FBQyxjQUFjLEVBQUUsTUFBTSxDQUFDO2FBQ2xDO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsTUFBTSxFQUFFLFlBQVk7Z0JBQ3BCLE9BQU8sRUFBRTtvQkFDUCxrQkFBa0IsRUFBRSxLQUFLO29CQUN6QixPQUFPLEVBQUUsQ0FBQyxzQkFBWSxFQUFFLENBQUM7aUJBQzFCO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsT0FBTztnQkFDYixNQUFNLEVBQUUsV0FBVztnQkFDbkIsT0FBTyxFQUFFO29CQUNQLGdCQUFnQixFQUFFLENBQUMsUUFBUSxDQUFDO2lCQUM3QjtnQkFDRCxPQUFPLEVBQUUsY0FBYzthQUN4QjtZQUNEO2dCQUNFLElBQUksRUFBRSxPQUFPO2dCQUNiLE1BQU0sRUFBRSxjQUFjO2dCQUN0QixPQUFPLEVBQUU7b0JBQ1AsY0FBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsb0NBQW9DLENBQUM7b0JBQzdELGNBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLFdBQVcsQ0FBQztvQkFDcEMsY0FBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsWUFBWSxDQUFDO2lCQUN0QzthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLHNCQUFzQjtnQkFDNUIsTUFBTSxFQUFFLGFBQWE7Z0JBQ3JCLE9BQU8sRUFBRTtvQkFDUCxJQUFJLEVBQUUscUJBQXFCO2lCQUM1QjthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsR0FBRyxFQUFFO29CQUNILGtCQUFrQjtvQkFDbEIsWUFBWTtvQkFDWixhQUFhO2lCQUNkO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsU0FBUztnQkFDZixHQUFHLEVBQUU7b0JBQ0gsa0JBQWtCO29CQUNsQixZQUFZO29CQUNaLGFBQWE7b0JBQ2IsYUFBYTtpQkFDZDthQUNGO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLFNBQVM7Z0JBQ2YsR0FBRyxFQUFFO29CQUNILGtCQUFrQjtvQkFDbEIsWUFBWTtvQkFDWixhQUFhO29CQUNiO3dCQUNFLE1BQU0sRUFBRSxhQUFhO3dCQUNyQixPQUFPLEVBQUU7NEJBQ1AsY0FBYyxFQUFFLElBQUk7eUJBQ3JCO3FCQUNGO2lCQUNGO2FBQ0Y7WUFDRDtnQkFDRSxJQUFJLEVBQUUsaUNBQWlDO2dCQUN2QyxNQUFNLEVBQUUsd0NBQXdDO2FBQ2pEO1lBQ0Q7Z0JBQ0UsSUFBSSxFQUFFLG1CQUFtQjtnQkFDekIsT0FBTyxFQUFFLGNBQWM7Z0JBQ3ZCLE1BQU0sRUFBRSxDQUFDLG9CQUFvQixDQUFDO2FBQy9CO1NBQ0Y7S0FDRjtDQUNGLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgcGF0aCBmcm9tICdwYXRoJztcbmltcG9ydCBjb25maWcgZnJvbSAnY29uZmlnJztcbmltcG9ydCBmcyBmcm9tICdmcyc7XG5pbXBvcnQgQ2FzZVNlbnNpdGl2ZVBhdGhzUGx1Z2luIGZyb20gJ2Nhc2Utc2Vuc2l0aXZlLXBhdGhzLXdlYnBhY2stcGx1Z2luJztcbmltcG9ydCBWdWVMb2FkZXJQbHVnaW4gZnJvbSAndnVlLWxvYWRlci9saWIvcGx1Z2luJztcbmltcG9ydCBhdXRvcHJlZml4ZXIgZnJvbSAnYXV0b3ByZWZpeGVyJztcbmltcG9ydCBIVE1MUGx1Z2luIGZyb20gJ2h0bWwtd2VicGFjay1wbHVnaW4nO1xuLy8gY29uc3QgQnVuZGxlQW5hbHl6ZXJQbHVnaW4gPSByZXF1aXJlKCd3ZWJwYWNrLWJ1bmRsZS1hbmFseXplcicpLkJ1bmRsZUFuYWx5emVyUGx1Z2luO1xuaW1wb3J0IHdlYnBhY2sgZnJvbSAnd2VicGFjayc7XG5pbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudCc7XG5cbmZzLndyaXRlRmlsZVN5bmMoXG4gIHBhdGgucmVzb2x2ZShfX2Rpcm5hbWUsICcuL2NvbmZpZy5qc29uJyksXG4gIEpTT04uc3RyaW5naWZ5KGNvbmZpZylcbilcblxuY29uc3QgdGhlbWVzUm9vdCA9ICcuLi8uLi9zcmMvdGhlbWVzJ1xuXG5pbXBvcnQgdGhlbWVSb290IGZyb20gJy4vdGhlbWUtcGF0aCc7XG5jb25zdCB0aGVtZVJlc291cmNlcyA9IHRoZW1lUm9vdCArICcvcmVzb3VyY2UnXG5jb25zdCB0aGVtZUNTUyA9IHRoZW1lUm9vdCArICcvY3NzJ1xuY29uc3QgdGhlbWVBcHAgPSB0aGVtZVJvb3QgKyAnL0FwcC52dWUnXG5jb25zdCB0aGVtZWRJbmRleCA9IHBhdGguam9pbih0aGVtZVJvb3QsICcvdGVtcGxhdGVzL2luZGV4LnRlbXBsYXRlLmh0bWwnKVxuY29uc3QgdGhlbWVkSW5kZXhNaW5pbWFsID0gcGF0aC5qb2luKHRoZW1lUm9vdCwgJy90ZW1wbGF0ZXMvaW5kZXgubWluaW1hbC50ZW1wbGF0ZS5odG1sJylcbmNvbnN0IHRoZW1lZEluZGV4QmFzaWMgPSBwYXRoLmpvaW4odGhlbWVSb290LCAnL3RlbXBsYXRlcy9pbmRleC5iYXNpYy50ZW1wbGF0ZS5odG1sJylcbmNvbnN0IHRoZW1lZEluZGV4QW1wID0gcGF0aC5qb2luKHRoZW1lUm9vdCwgJy90ZW1wbGF0ZXMvaW5kZXguYW1wLnRlbXBsYXRlLmh0bWwnKVxuXG5jb25zdCB0cmFuc2xhdGlvblByZXByb2Nlc3NvciA9IHJlcXVpcmUoJ0B2dWUtc3RvcmVmcm9udC9pMThuL3NjcmlwdHMvdHJhbnNsYXRpb24ucHJlcHJvY2Vzc29yLmpzJylcbnRyYW5zbGF0aW9uUHJlcHJvY2Vzc29yKFtcbiAgcGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgJy4uLy4uL25vZGVfbW9kdWxlcy9AdnVlLXN0b3JlZnJvbnQvaTE4bi9yZXNvdXJjZS9pMThuLycpLFxuICBwYXRoLnJlc29sdmUoX19kaXJuYW1lLCB0aGVtZVJlc291cmNlcyArICcvaTE4bi8nKVxuXSwgY29uZmlnKVxuXG5jb25zdCBwb3N0Y3NzQ29uZmlnID0gIHtcbiAgbG9hZGVyOiAncG9zdGNzcy1sb2FkZXInLFxuICBvcHRpb25zOiB7XG4gICAgaWRlbnQ6ICdwb3N0Y3NzJyxcbiAgICBwbHVnaW5zOiAobG9hZGVyKSA9PiBbXG4gICAgICByZXF1aXJlKCdwb3N0Y3NzLWZsZXhidWdzLWZpeGVzJyksXG4gICAgICByZXF1aXJlKCdhdXRvcHJlZml4ZXInKSh7XG4gICAgICAgIGZsZXhib3g6ICduby0yMDA5JyxcbiAgICAgIH0pLFxuICAgIF1cbiAgfVxufTtcbmNvbnN0IGlzUHJvZCA9IHByb2Nlc3MuZW52Lk5PREVfRU5WID09PSAncHJvZHVjdGlvbidcbi8vIHRvZG86IHVzZW11bHRpcGFnZS13ZWJwYWNrLXBsdWdpbiBmb3IgbXVsdGlzdG9yZVxuZXhwb3J0IGRlZmF1bHQge1xuICBwbHVnaW5zOiBbXG4gICAgbmV3IHdlYnBhY2suUHJvZ3Jlc3NQbHVnaW4oKSxcbiAgICAvLyBuZXcgQnVuZGxlQW5hbHl6ZXJQbHVnaW4oe1xuICAgIC8vICAgZ2VuZXJhdGVTdGF0c0ZpbGU6IHRydWVcbiAgICAvLyB9KSxcbiAgICBuZXcgQ2FzZVNlbnNpdGl2ZVBhdGhzUGx1Z2luKCksXG4gICAgbmV3IFZ1ZUxvYWRlclBsdWdpbigpLFxuICAgIC8vIGdlbmVyYXRlIG91dHB1dCBIVE1MXG4gICAgbmV3IEhUTUxQbHVnaW4oe1xuICAgICAgdGVtcGxhdGU6IGZzLmV4aXN0c1N5bmModGhlbWVkSW5kZXgpID8gdGhlbWVkSW5kZXggOiAnc3JjL2luZGV4LnRlbXBsYXRlLmh0bWwnLFxuICAgICAgZmlsZW5hbWU6ICdpbmRleC5odG1sJyxcbiAgICAgIGNodW5rc1NvcnRNb2RlOiAnbm9uZScsXG4gICAgICBpbmplY3Q6IGlzUHJvZCA9PSBmYWxzZSAvLyBpbiBkZXYgbW9kZSB3ZSdyZSBub3QgdXNpbmcgY2xpZW50TWFuaWZlc3QgdGhlcmVmb3JlIHJlbmRlclNjcmlwdHMoKSBpcyByZXR1cm5pbmcgZW1wdHkgc3RyaW5nIGFuZCB3ZSBuZWVkIHRvIGluamVjdCBzY3JpcHRzIHVzaW5nIEhUTUxQbHVnaW5cbiAgICB9KSxcbiAgICBuZXcgSFRNTFBsdWdpbih7XG4gICAgICB0ZW1wbGF0ZTogZnMuZXhpc3RzU3luYyh0aGVtZWRJbmRleE1pbmltYWwpID8gdGhlbWVkSW5kZXhNaW5pbWFsIDogJ3NyYy9pbmRleC5taW5pbWFsLnRlbXBsYXRlLmh0bWwnLFxuICAgICAgZmlsZW5hbWU6ICdpbmRleC5taW5pbWFsLmh0bWwnLFxuICAgICAgY2h1bmtzU29ydE1vZGU6ICdub25lJyxcbiAgICAgIGluamVjdDogaXNQcm9kID09IGZhbHNlXG4gICAgfSksXG4gICAgbmV3IEhUTUxQbHVnaW4oe1xuICAgICAgdGVtcGxhdGU6IGZzLmV4aXN0c1N5bmModGhlbWVkSW5kZXhCYXNpYykgPyB0aGVtZWRJbmRleEJhc2ljOiAnc3JjL2luZGV4LmJhc2ljLnRlbXBsYXRlLmh0bWwnLFxuICAgICAgZmlsZW5hbWU6ICdpbmRleC5iYXNpYy5odG1sJyxcbiAgICAgIGNodW5rc1NvcnRNb2RlOiAnbm9uZScsXG4gICAgICBpbmplY3Q6IGlzUHJvZCA9PSBmYWxzZVxuICAgIH0pLFxuICAgIG5ldyBIVE1MUGx1Z2luKHtcbiAgICAgIHRlbXBsYXRlOiBmcy5leGlzdHNTeW5jKHRoZW1lZEluZGV4QW1wKSA/IHRoZW1lZEluZGV4QW1wOiAnc3JjL2luZGV4LmFtcC50ZW1wbGF0ZS5odG1sJyxcbiAgICAgIGZpbGVuYW1lOiAnaW5kZXguYW1wLmh0bWwnLFxuICAgICAgY2h1bmtzU29ydE1vZGU6ICdub25lJyxcbiAgICAgIGluamVjdDogaXNQcm9kID09IGZhbHNlXG4gICAgfSksXG4gICAgbmV3IHdlYnBhY2suRGVmaW5lUGx1Z2luKHtcbiAgICAgICdwcm9jZXNzLmVudi5fX0FQUFZFUlNJT05fXyc6IEpTT04uc3RyaW5naWZ5KHJlcXVpcmUoJy4uLy4uL3BhY2thZ2UuanNvbicpLnZlcnNpb24pLFxuICAgICAgJ3Byb2Nlc3MuZW52Ll9fQlVJTERUSU1FX18nOiBKU09OLnN0cmluZ2lmeShtb21lbnQoKS5mb3JtYXQoJ1lZWVktTU0tREQgSEg6bW06c3MnKSlcbiAgICB9KVxuICBdLFxuICBkZXZ0b29sOiAnc291cmNlLW1hcCcsXG4gIGVudHJ5OiB7XG4gICAgYXBwOiBbJ0BiYWJlbC9wb2x5ZmlsbCcsICcuL2NvcmUvY2xpZW50LWVudHJ5LnRzJ11cbiAgfSxcbiAgb3V0cHV0OiB7XG4gICAgcGF0aDogcGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgJy4uLy4uL2Rpc3QnKSxcbiAgICBwdWJsaWNQYXRoOiAnL2Rpc3QvJyxcbiAgICBmaWxlbmFtZTogJ1tuYW1lXS5baGFzaF0uanMnXG4gIH0sXG4gIHJlc29sdmVMb2FkZXI6IHtcbiAgICBtb2R1bGVzOiBbXG4gICAgICAnbm9kZV9tb2R1bGVzJyxcbiAgICAgIHBhdGgucmVzb2x2ZShfX2Rpcm5hbWUsIHRoZW1lc1Jvb3QpXG4gICAgXSxcbiAgfSxcbiAgcmVzb2x2ZToge1xuICAgIG1vZHVsZXM6IFtcbiAgICAgICdub2RlX21vZHVsZXMnLFxuICAgICAgcGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgdGhlbWVzUm9vdClcbiAgICBdLFxuICAgIGV4dGVuc2lvbnM6IFsnLmpzJywgJy52dWUnLCAnLmdxbCcsICcuZ3JhcGhxbHMnLCAnLnRzJ10sXG4gICAgYWxpYXM6IHtcbiAgICAgIC8vIE1haW4gYWxpYXNlc1xuICAgICAgJ2NvbmZpZyc6IHBhdGgucmVzb2x2ZShfX2Rpcm5hbWUsICcuL2NvbmZpZy5qc29uJyksXG4gICAgICAnc3JjJzogcGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgJy4uLy4uL3NyYycpLFxuXG4gICAgICAvLyBUaGVtZSBhbGlhc2VzXG4gICAgICAndGhlbWUnOiB0aGVtZVJvb3QsXG4gICAgICAndGhlbWUvYXBwJzogdGhlbWVBcHAsXG4gICAgICAndGhlbWUvY3NzJzogdGhlbWVDU1MsXG4gICAgICAndGhlbWUvcmVzb3VyY2UnOiB0aGVtZVJlc291cmNlcyxcblxuICAgICAgLy8gQmFja3dhcmQgY29tcGF0aWJsZVxuICAgICAgJ0B2dWUtc3RvcmVmcm9udC9jb3JlL3N0b3JlL2xpYi9tdWx0aXN0b3JlJyA6IHBhdGgucmVzb2x2ZShfX2Rpcm5hbWUsICcuLi9saWIvbXVsdGlzdG9yZS50cycpLFxuICAgIH1cbiAgfSxcbiAgbW9kdWxlOiB7XG4gICAgcnVsZXM6IFtcbiAgICAgIHtcbiAgICAgICAgZW5mb3JjZTogJ3ByZScsXG4gICAgICAgIHRlc3Q6IC9cXC4oanN8dnVlKSQvLFxuICAgICAgICBsb2FkZXI6ICdlc2xpbnQtbG9hZGVyJyxcbiAgICAgICAgZXhjbHVkZTogWy9ub2RlX21vZHVsZXMvLCAvdGVzdC9dXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0ZXN0OiAvXFwudnVlJC8sXG4gICAgICAgIGxvYWRlcjogJ3Z1ZS1sb2FkZXInLFxuICAgICAgICBvcHRpb25zOiB7XG4gICAgICAgICAgcHJlc2VydmVXaGl0ZXNwYWNlOiBmYWxzZSxcbiAgICAgICAgICBwb3N0Y3NzOiBbYXV0b3ByZWZpeGVyKCldLFxuICAgICAgICB9XG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0ZXN0OiAvXFwudHMkLyxcbiAgICAgICAgbG9hZGVyOiAndHMtbG9hZGVyJyxcbiAgICAgICAgb3B0aW9uczoge1xuICAgICAgICAgIGFwcGVuZFRzU3VmZml4VG86IFsvXFwudnVlJC9dXG4gICAgICAgIH0sXG4gICAgICAgIGV4Y2x1ZGU6IC9ub2RlX21vZHVsZXMvXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0ZXN0OiAvXFwuanMkLyxcbiAgICAgICAgbG9hZGVyOiAnYmFiZWwtbG9hZGVyJyxcbiAgICAgICAgaW5jbHVkZTogW1xuICAgICAgICAgIHBhdGgucmVzb2x2ZShfX2Rpcm5hbWUsICcuLi8uLi9ub2RlX21vZHVsZXMvQHZ1ZS1zdG9yZWZyb250JyksXG4gICAgICAgICAgcGF0aC5yZXNvbHZlKF9fZGlybmFtZSwgJy4uLy4uL3NyYycpLFxuICAgICAgICAgIHBhdGgucmVzb2x2ZShfX2Rpcm5hbWUsICcuLi8uLi9jb3JlJylcbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdGVzdDogL1xcLihwbmd8anBnfGdpZnxzdmcpJC8sXG4gICAgICAgIGxvYWRlcjogJ2ZpbGUtbG9hZGVyJyxcbiAgICAgICAgb3B0aW9uczoge1xuICAgICAgICAgIG5hbWU6ICdbbmFtZV0uW2V4dF0/W2hhc2hdJ1xuICAgICAgICB9XG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0ZXN0OiAvXFwuY3NzJC8sXG4gICAgICAgIHVzZTogW1xuICAgICAgICAgICd2dWUtc3R5bGUtbG9hZGVyJyxcbiAgICAgICAgICAnY3NzLWxvYWRlcicsXG4gICAgICAgICAgcG9zdGNzc0NvbmZpZ1xuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0ZXN0OiAvXFwuc2NzcyQvLFxuICAgICAgICB1c2U6IFtcbiAgICAgICAgICAndnVlLXN0eWxlLWxvYWRlcicsXG4gICAgICAgICAgJ2Nzcy1sb2FkZXInLFxuICAgICAgICAgIHBvc3Rjc3NDb25maWcsXG4gICAgICAgICAgJ3Nhc3MtbG9hZGVyJ1xuICAgICAgICBdXG4gICAgICB9LFxuICAgICAge1xuICAgICAgICB0ZXN0OiAvXFwuc2FzcyQvLFxuICAgICAgICB1c2U6IFtcbiAgICAgICAgICAndnVlLXN0eWxlLWxvYWRlcicsXG4gICAgICAgICAgJ2Nzcy1sb2FkZXInLFxuICAgICAgICAgIHBvc3Rjc3NDb25maWcsXG4gICAgICAgICAge1xuICAgICAgICAgICAgbG9hZGVyOiAnc2Fzcy1sb2FkZXInLFxuICAgICAgICAgICAgb3B0aW9uczoge1xuICAgICAgICAgICAgICBpbmRlbnRlZFN5bnRheDogdHJ1ZVxuICAgICAgICAgICAgfVxuICAgICAgICAgIH1cbiAgICAgICAgXVxuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdGVzdDogL1xcLih3b2ZmfHdvZmYyfGVvdHx0dGYpKFxcPy4qJHwkKS8sXG4gICAgICAgIGxvYWRlcjogJ3VybC1sb2FkZXI/aW1wb3J0TG9hZGVycz0xJmxpbWl0PTEwMDAwJ1xuICAgICAgfSxcbiAgICAgIHtcbiAgICAgICAgdGVzdDogL1xcLihncmFwaHFsc3xncWwpJC8sXG4gICAgICAgIGV4Y2x1ZGU6IC9ub2RlX21vZHVsZXMvLFxuICAgICAgICBsb2FkZXI6IFsnZ3JhcGhxbC10YWcvbG9hZGVyJ11cbiAgICAgIH1cbiAgICBdXG4gIH1cbn1cbiJdfQ==