"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var webpack_1 = __importDefault(require("webpack"));
var webpack_merge_1 = __importDefault(require("webpack-merge"));
var webpack_base_config_1 = __importDefault(require("./webpack.base.config"));
var client_plugin_1 = __importDefault(require("vue-server-renderer/client-plugin"));
// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
var config = webpack_merge_1.default(webpack_base_config_1.default, {
    optimization: {
        splitChunks: {
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/](vue|vuex|vue-router|vue-meta|vue-i18n|vuex-router-sync|vue-meta|localforage)[\\/]/,
                    name: 'vendor',
                    chunks: 'all',
                },
            },
        },
        runtimeChunk: {
            name: "manifest",
        }
    },
    mode: 'development',
    resolve: {
        alias: {
            'create-api': './create-api-client.js'
        }
    },
    plugins: [
        // new BundleAnalyzerPlugin(),
        // strip dev-only code in Vue source
        new webpack_1.default.DefinePlugin({
            'process.env.VUE_ENV': '"client"'
        }),
        new client_plugin_1.default()
    ],
});
exports.default = config;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiL2hvbWUvYW5kcmV5L3Byb2plY3QvdnVlLXN0b3JlZnJvbnQvdnVlLXN0b3JlZnJvbnQvY29yZS9idWlsZC93ZWJwYWNrLmNsaWVudC5jb25maWcudHMiLCJzb3VyY2VzIjpbIi9ob21lL2FuZHJleS9wcm9qZWN0L3Z1ZS1zdG9yZWZyb250L3Z1ZS1zdG9yZWZyb250L2NvcmUvYnVpbGQvd2VicGFjay5jbGllbnQuY29uZmlnLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsb0RBQTZCO0FBQzdCLGdFQUFpQztBQUNqQyw4RUFBd0M7QUFDeEMsb0ZBQWtFO0FBQ2xFLHVGQUF1RjtBQUV2RixJQUFNLE1BQU0sR0FBRyx1QkFBSyxDQUFDLDZCQUFJLEVBQUU7SUFDekIsWUFBWSxFQUFFO1FBQ1osV0FBVyxFQUFHO1lBQ1osV0FBVyxFQUFFO2dCQUNYLE9BQU8sRUFBRTtvQkFDUCxJQUFJLEVBQUUsMEdBQTBHO29CQUNoSCxJQUFJLEVBQUUsUUFBUTtvQkFDZCxNQUFNLEVBQUUsS0FBSztpQkFDZDthQUNGO1NBQ0Y7UUFDQyxZQUFZLEVBQUU7WUFDZCxJQUFJLEVBQUUsVUFBVTtTQUNqQjtLQUNGO0lBQ0QsSUFBSSxFQUFFLGFBQWE7SUFDbkIsT0FBTyxFQUFFO1FBQ1AsS0FBSyxFQUFFO1lBQ0wsWUFBWSxFQUFFLHdCQUF3QjtTQUN2QztLQUNGO0lBQ0QsT0FBTyxFQUFFO1FBQ1AsOEJBQThCO1FBQzlCLG9DQUFvQztRQUNwQyxJQUFJLGlCQUFPLENBQUMsWUFBWSxDQUFDO1lBQ3ZCLHFCQUFxQixFQUFFLFVBQVU7U0FDbEMsQ0FBQztRQUNGLElBQUksdUJBQWtCLEVBQUU7S0FDekI7Q0FDRixDQUFDLENBQUE7QUFFRixrQkFBZSxNQUFNLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgd2VicGFjayBmcm9tICd3ZWJwYWNrJ1xuaW1wb3J0IG1lcmdlIGZyb20gJ3dlYnBhY2stbWVyZ2UnXG5pbXBvcnQgYmFzZSBmcm9tICcuL3dlYnBhY2suYmFzZS5jb25maWcnXG5pbXBvcnQgVnVlU1NSQ2xpZW50UGx1Z2luIGZyb20gJ3Z1ZS1zZXJ2ZXItcmVuZGVyZXIvY2xpZW50LXBsdWdpbidcbi8vIGNvbnN0IEJ1bmRsZUFuYWx5emVyUGx1Z2luID0gcmVxdWlyZSgnd2VicGFjay1idW5kbGUtYW5hbHl6ZXInKS5CdW5kbGVBbmFseXplclBsdWdpblxuXG5jb25zdCBjb25maWcgPSBtZXJnZShiYXNlLCB7XG4gIG9wdGltaXphdGlvbjoge1xuICAgIHNwbGl0Q2h1bmtzOiAge1xuICAgICAgY2FjaGVHcm91cHM6IHtcbiAgICAgICAgY29tbW9uczoge1xuICAgICAgICAgIHRlc3Q6IC9bXFxcXC9dbm9kZV9tb2R1bGVzW1xcXFwvXSh2dWV8dnVleHx2dWUtcm91dGVyfHZ1ZS1tZXRhfHZ1ZS1pMThufHZ1ZXgtcm91dGVyLXN5bmN8dnVlLW1ldGF8bG9jYWxmb3JhZ2UpW1xcXFwvXS8sXG4gICAgICAgICAgbmFtZTogJ3ZlbmRvcicsXG4gICAgICAgICAgY2h1bmtzOiAnYWxsJyxcbiAgICAgICAgfSxcbiAgICAgIH0sXG4gICAgfSxcbiAgICAgIHJ1bnRpbWVDaHVuazoge1xuICAgICAgbmFtZTogXCJtYW5pZmVzdFwiLFxuICAgIH1cbiAgfSxcbiAgbW9kZTogJ2RldmVsb3BtZW50JyxcbiAgcmVzb2x2ZToge1xuICAgIGFsaWFzOiB7XG4gICAgICAnY3JlYXRlLWFwaSc6ICcuL2NyZWF0ZS1hcGktY2xpZW50LmpzJ1xuICAgIH1cbiAgfSxcbiAgcGx1Z2luczogW1xuICAgIC8vIG5ldyBCdW5kbGVBbmFseXplclBsdWdpbigpLFxuICAgIC8vIHN0cmlwIGRldi1vbmx5IGNvZGUgaW4gVnVlIHNvdXJjZVxuICAgIG5ldyB3ZWJwYWNrLkRlZmluZVBsdWdpbih7XG4gICAgICAncHJvY2Vzcy5lbnYuVlVFX0VOVic6ICdcImNsaWVudFwiJ1xuICAgIH0pLFxuICAgIG5ldyBWdWVTU1JDbGllbnRQbHVnaW4oKVxuICBdLFxufSlcblxuZXhwb3J0IGRlZmF1bHQgY29uZmlnO1xuIl19